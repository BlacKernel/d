#!/bin/bash

function init {
	export CHAR_SHEET="$HOME/.config/char/char.xml"
	if [ -a "$CHAR_SHEET" ];then
		mv "$CHAR_SHEET" "$CHAR_SHEET.old"
	else
		mkdir -p "$HOME/.config/char"
		touch "$HOME/.config/char/char.xml"
		if [ -z "$(cat "$HOME/.bashrc" | grep "CHAR_SHEET=")" ];then
			echo "CHAR_SHEET=\"$HOME/.config/char/char.xml\"" >> "$HOME/.bashrc"
		fi
	fi

	echo "<char></char>" > "$CHAR_SHEET"

	xmlstarlet ed -L -s "/char" -t elem -n "name"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "race"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "class"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "level"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "xp"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "ac"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "hp_max"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "hp"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "hd_max"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "hd"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "str"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "dex"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "con"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "int"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "wis"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "cha"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "inv"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv" -t elem -n "equipped"	"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv" -t elem -n "weapons"	"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv" -t elem -n "armors"	"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv" -t elem -n "money"	"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv" -t elem -n "misc"	"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "spells"		"$CHAR_SHEET"
	xmlstarlet ed -L -s "/char" -t elem -n "other"		"$CHAR_SHEET"
}

function init_2e {
	selection=$(dialog --stdout --title "Choose Ability Score Meathod" --radiolist \
		"Which meathod would you like \n
		to use to generate ability scores?" 20 50 4 \
		"Meathod 1" "3d6 in order" on \
		"Meathod 2" "3d6 in order at advantage" off \
		"Meathod 3" "3d6 choose ability" off \
		"Meathod 4" "3d6 choose ability at advantage" off)

	cp "$CHAR_SHEET" "$CHAR_SHEET.bak"

	if [ "$selection" = "Meathod 1" ];then
		3d6_in_order
	elif [ "$selection" = "Meathod 2" ];then
		3d6_advantage
	elif [ "$selection" = "Meathod 3" ];then
		printf ""
	elif [ "$selection" = "Meathod 4" ];then
		printf ""
	fi

	unset selection
	dialog --stdout --title "Confirm Ability Scores" --yesno \
		"
		STR: $(v -s char.str) \n
		 DEX: $(v -s char.dex) \n
		 CON: $(v -s char.con) \n
		 INT: $(v -s char.int) \n
		 WIS: $(v -s char.wis) \n
		 CHA: $(v -s char.cha)" 20 50
	confirm=$?

	if [ "$confirm" != 0 ];then
		mv "$CHAR_SHEET.bak" "$CHAR_SHEET"
		clear
		exit $confirm
	fi
	unset confirm

	races=()

	races+=("Human" "No Adjustment" on)

	if \
	(( $(v -s char.str) >= 8  )) && (( $(v -s char.str) <= 18 )) && \
	(( $(v -s char.dex) >= 3  )) && (( $(v -s char.dex) <= 17 )) && \
	(( $(v -s char.con) >= 11 )) && (( $(v -s char.con) <= 18 )) && \
	(( $(v -s char.int) >= 3  )) && (( $(v -s char.int) <= 18 )) && \
	(( $(v -s char.wis) >= 3  )) && (( $(v -s char.wis) <= 18 )) && \
	(( $(v -s char.cha) >= 3  )) && (( $(v -s char.cha) <= 17 )); then
		races+=("Dwarf" "CON +1, CHA -1" off)
	fi

	if \
	(( $(v -s char.str) >= 3  )) && (( $(v -s char.str) <= 18 )) && \
	(( $(v -s char.dex) >= 6  )) && (( $(v -s char.dex) <= 18 )) && \
	(( $(v -s char.con) >= 7  )) && (( $(v -s char.con) <= 18 )) && \
	(( $(v -s char.int) >= 8  )) && (( $(v -s char.int) <= 18 )) && \
	(( $(v -s char.wis) >= 3  )) && (( $(v -s char.wis) <= 18 )) && \
	(( $(v -s char.cha) >= 8  )) && (( $(v -s char.cha) <= 18 )); then
		races+=("Elf" "DEX +1, CON -1" off)
	fi

	if \
	(( $(v -s char.str) >= 6  )) && (( $(v -s char.str) <= 18 )) && \
	(( $(v -s char.dex) >= 3  )) && (( $(v -s char.dex) <= 18 )) && \
	(( $(v -s char.con) >= 8  )) && (( $(v -s char.con) <= 18 )) && \
	(( $(v -s char.int) >= 6  )) && (( $(v -s char.int) <= 18 )) && \
	(( $(v -s char.wis) >= 3  )) && (( $(v -s char.wis) <= 18 )) && \
	(( $(v -s char.cha) >= 3  )) && (( $(v -s char.cha) <= 18 )); then
		races+=("Gnome" "INT +1, WIS -1" off)
	fi

	if \
	(( $(v -s char.str) >= 3  )) && (( $(v -s char.str) <= 18 )) && \
	(( $(v -s char.dex) >= 6  )) && (( $(v -s char.dex) <= 18 )) && \
	(( $(v -s char.con) >= 6  )) && (( $(v -s char.con) <= 18 )) && \
	(( $(v -s char.int) >= 4  )) && (( $(v -s char.int) <= 18 )) && \
	(( $(v -s char.wis) >= 3  )) && (( $(v -s char.wis) <= 18 )) && \
	(( $(v -s char.cha) >= 3  )) && (( $(v -s char.cha) <= 18 )); then
		races+=("Half-Elf" "No Adjustment" off)
	fi

	if \
	(( $(v -s char.str) >= 7  )) && (( $(v -s char.str) <= 18 )) && \
	(( $(v -s char.dex) >= 7  )) && (( $(v -s char.dex) <= 18 )) && \
	(( $(v -s char.con) >= 10 )) && (( $(v -s char.con) <= 18 )) && \
	(( $(v -s char.int) >= 6  )) && (( $(v -s char.int) <= 18 )) && \
	(( $(v -s char.wis) >= 3  )) && (( $(v -s char.wis) <= 17 )) && \
	(( $(v -s char.cha) >= 3  )) && (( $(v -s char.cha) <= 18 )); then
		races+=("Halfling" "DEX +1, STR -1" off)
	fi

	selection=$(dialog --stdout --title "Choose Race" --radiolist \
		"Which race would you like \n
		to choose for your character?" 20 50 $((${#races}+1)) \
		"${races[@]}"
	)

	cp "$CHAR_SHEET" "$CHAR_SHEET.bak"

	if [ "$selection" = "Dwarf" ];then
		xmlstarlet ed -L -u "/char/con" -v "$(echo "$(v -s char.con) + 1" | bc)" "$CHAR_SHEET"
		xmlstarlet ed -L -u "/char/cha" -v "$(echo "$(v -s char.cha) - 1" | bc)" "$CHAR_SHEET"
		xmlstarlet ed -L -u "/char/race" -v "$selection" "$CHAR_SHEET"
	elif [ "$selection" = "Elf" ];then
		xmlstarlet ed -L -u "/char/dex" -v "$(echo "$(v -s char.dex) + 1" | bc)" "$CHAR_SHEET"
		xmlstarlet ed -L -u "/char/con" -v "$(echo "$(v -s char.con) - 1" | bc)" "$CHAR_SHEET"
		xmlstarlet ed -L -u "/char/race" -v "$selection" "$CHAR_SHEET"
	elif [ "$selection" = "Gnome" ];then
		xmlstarlet ed -L -u "/char/int" -v "$(echo "$(v -s char.int) + 1" | bc)" "$CHAR_SHEET"
		xmlstarlet ed -L -u "/char/wis" -v "$(echo "$(v -s char.wis) + 1" | bc)" "$CHAR_"
		xmlstarlet ed -L -u "/char/race" -v "$selection" "$CHAR_SHEET"
	elif [ "$selection" = "Halfling" ];then
		xmlstarlet ed -L -u "/char/dex" -v "$(echo "$(v -s char.dex) + 1" | bc)" "$CHAR_SHEET"
		xmlstarlet ed -L -u "/char/str" -v "$(echo "$(v -s char.str) - 1" | bc)" "$CHAR_SHEET"
		xmlstarlet ed -L -u "/char/race" -v "$selection" "$CHAR_SHEET"
	else
		xmlstarlet ed -L -u "/char/race" -v "$selection" "$CHAR_SHEET"
	fi

	unset races
	unset selection
	dialog --stdout --title "Confirm Race & Ability Scores" --yesno \
		"
		Race: $(v -s char.race)\n
		 STR: $(v -s char.str) \n
		 DEX: $(v -s char.dex) \n
		 CON: $(v -s char.con) \n
		 INT: $(v -s char.int) \n
		 WIS: $(v -s char.wis) \n
		 CHA: $(v -s char.cha)" 20 50
	confirm=$?

	if [ "$confirm" != 0 ];then
		mv "$CHAR_SHEET.bak" "$CHAR_SHEET"
		clear
		exit $confirm
	fi
	unset confirm

	declare -A max_levels
	classes=()
	if \
	(( $(v -s char.str) >= 9 ));then
		if [ "$(v -s char.race)" = "Dwarf" ];then
			classes+=("Fighter" "Max Level: 15" off)
			max_levels+=([fighter]=15)
		elif [ "$(v -s char.race)" = "Elf" ];then
			classes+=("Fighter" "Max Level: 12" off)
			max_levels+=([fighter]=12)
		elif [ "$(v -s char.race)" = "Gnome" ];then
			classes+=("Fighter" "Max Level: 11" off)
			max_levels+=([fighter]=11)
		elif [ "$(v -s char.race)" = "Half-Elf" ];then
			classes+=("Fighter" "Max Level: 14" off)
			max_levels+=([fighter]=14)
		elif [ "$(v -s char.race)" = "Halfling" ];then
			classes+=("Fighter" "Max Level: 9" off)
			max_levels+=([fighter]=9)
		elif [ "$(v -s char.race)" = "Human" ];then
			classes+=("Fighter" "No Max Level" off)
			max_levels+=([fighter]="No Max Level")
		fi
	fi

	if \
	(( $(v -s char.str) >= 12 )) && \
	(( $(v -s char.con) >= 9  )) && \
	(( $(v -s char.wis) >= 13 )) && \
	(( $(v -s char.cha) >= 17 )) && \
	[ "$(v -s char.race)" = "Human" ];then
		classes+=("Paladin" "No Max Level" off)
		max_levels+=([paladin]="No Max Level")
	fi

	if \
	(( $(v -s char.str) >= 13 )) && \
	(( $(v -s char.dex) >= 13 )) && \
	(( $(v -s char.con) >= 14 )) && \
	(( $(v -s char.wis) >= 14 ));then
		if [ "$(v -s char.race)" = "Elf" ];then
			classes+=("Ranger" "Max Level: 15" off)
			max_levels+=([ranger]=15)
		elif [ "$(v -s char.race)" = "Half-Elf" ];then
			classes+=("Ranger" "Max Level: 16" off)
			max_levels+=([ranger]=16)
		elif [ "$(v -s char.race)" = "Human" ];then
			classes+=("Ranger" "No Max Level" off)
			max_levels+=([ranger]="No Max Level")
		fi
	fi

	if \
	(( $(v -s char.int) >= 9 ));then
		if [ "$(v -s char.race)" = "Elf" ];then
			classes+=("Wizard" "Max Level: 15" off)
			max_levels+=([wizard]=15)
		elif [ "$(v -s char.race)" = "Gnome" ];then
			classes+=("Wizard" "Max Level: 15" off)
			max_levels+=([wizard]=15)
		elif [ "$(v -s char.race)" = "Half-Elf" ];then
			classes+=("Wizard" "Max Level: 12" off)
			max_levels+=([wizard]=12)
		elif [ "$(v -s char.race)" = "Human" ];then
			classes+=("Wizard" "No Max Level" off)
			max_levels+=([wizard]="No Max Level")
		fi
	fi

	if \
	(( $(v -s char.wis) >= 9 ));then
		if [ "$(v -s char.race)" = "Dwarf" ];then
			classes+=("Cleric" "Max Level: 10" off)
			max_levels+=([cleric]=10)
		elif [ "$(v -s char.race)" = "Elf" ];then
			classes+=("Cleric" "Max Level: 12" off)
			max_levels+=([cleric]=12)
		elif [ "$(v -s char.race)" = "Gnome" ];then
			classes+=("Cleric" "Max Level: 9" off)
			max_levels+=([cleric]=9)
		elif [ "$(v -s char.race)" = "Half-Elf" ];then
			classes+=("Cleric" "Max Level: 14" off)
			max_levels+=([cleric]=14)
		elif [ "$(v -s char.race)" = "Halfling" ];then
			classes+=("Cleric" "Max Level: 8" off)
			max_levels+=([cleric]=8)
		elif [ "$(v -s char.race)" = "Human" ];then
			classes+=("Cleric" "No Max Level" off)
			max_levels+=([cleric]="No Max Level")
		fi
	fi

	if \
	(( $(v -s char.wis) >= 12 )) && \
	(( $(v -s char.cha) >= 15 ));then
		if [ "$(v -s char.race)" = "Half-Elf" ];then
			classes+=("Druid" "Max Level: 9" off)
			max_levels+=([druid]=9)
		elif [ "$(v -s char.race)" = "Human" ];then
			classes+=("Druid" "No Max Level" off)
			max_levels+=([druid]="No Max Level")
		fi
	fi

	if \
	(( $(v -s char.dex) >= 9 ));then
		if [ "$(v -s char.race)" = "Dwarf" ];then
			classes+=("Theif" "Max Level: 12" off)
			max_levels+=([theif]=12)
		elif [ "$(v -s char.race)" = "Elf" ];then
			classes+=("Theif" "Max Level: 12" off)
			max_levels+=([theif]=12)
		elif [ "$(v -s char.race)" = "Gnome" ];then
			classes+=("Theif" "Max Level: 13" off)
			max_levels+=([theif]=13)
		elif [ "$(v -s char.race)" = "Half-Elf" ];then
			classes+=("Theif" "Max Level: 12" off)
			max_levels+=([theif]=12)
		elif [ "$(v -s char.race)" = "Halfling" ];then
			classes+=("Theif" "Max Level: 15" off)
			max_levels+=([theif]=15)
		elif [ "$(v -s char.race)" = "Human" ];then
			classes+=("Theif" "No Max Level" off)
			max_levels+=([theif]="No Max Level")
		fi
	fi

	if \
	(( $(v -s char.dex) >= 12 )) && \
	(( $(v -s char.int) >= 13 )) && \
	(( $(v -s char.cha) >= 15 ));then
		if [ "$(v -s char.race)" = "Half-Elf" ];then
			classes+=("Bard" "No Max Level" off)
			max_levels+=([bard]="No Max Level")
		elif [ "$(v -s char.race)" = "Human" ];then
			classes+=("Bard" "No Max Level" off)
			max_levels+=([bard]="No Max Level")
		fi
	fi

	selection=
	while [ -z "$selection" ];do
		selection=$(dialog --stdout --title "Choose Class" --radiolist \
			"Which class would you like \n
			to choose for your character?" 20 50 $((${#classes}+1)) \
			"${classes[@]}"
		)
	done

	cp "$CHAR_SHEET" "$CHAR_SHEET.bak"

	xmlstarlet ed -L -u "/char/class" -v "$selection" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/other" -t elem -n "max-level" -v "$(v -s char.class) - ${max_levels["${selection,,}"]}" "$CHAR_SHEET"

	unset classes
	unset selection
	dialog --stdout --title "Confirm Race & Ability Scores" --yesno \
		"
		Race: $(v -s char.race)\n
		 Class: $(v -s char.class) \n
		 STR: $(v -s char.str) \n
		 DEX: $(v -s char.dex) \n
		 CON: $(v -s char.con) \n
		 INT: $(v -s char.int) \n
		 WIS: $(v -s char.wis) \n
		 CHA: $(v -s char.cha) \n
		 Other: \n
		     Max Level: $(v -s char.other.max-level)" 20 50
	confirm=$?

	if [ "$confirm" != 0 ];then
		mv "$CHAR_SHEET.bak" "$CHAR_SHEET"
		clear
		exit $confirm
	fi
	unset confirm

	cp "$CHAR_SHEET" "$CHAR_SHEET.bak"

	if (( $(v -s char.str) = 1 ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "-5" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "-4" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "1 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "3 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "1 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "0" "$CHAR_SHEET"
	elif (( $(v -s char.str) = 2   ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "-3" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "-2" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "1 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "5 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "1 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "0" "$CHAR_SHEET"
	elif (( $(v -s char.str) = 3   ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "-3" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "-1" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "5 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "10 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "2 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "0" "$CHAR_SHEET"
	elif (( $(v -s char.str) <= 5  ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "-2" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "-1" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "10 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "25 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "3 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "0" "$CHAR_SHEET"
	elif (( $(v -s char.str) <= 7  ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "-1" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "20 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "55 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "4 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "0" "$CHAR_SHEET"
	elif (( $(v -s char.str) <= 9  ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "35 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "90 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "5 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "1" "$CHAR_SHEET"
	elif (( $(v -s char.str) <= 11 ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "40 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "115 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "6 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "2" "$CHAR_SHEET"
	elif (( $(v -s char.str) <= 13 ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "45 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "140 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "7 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "4" "$CHAR_SHEET"
	elif (( $(v -s char.str) <= 15 ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "55 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "170 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "8 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "7" "$CHAR_SHEET"
	elif (( $(v -s char.str) = 16  ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+0" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+1" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "70 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "195 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "9 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "10" "$CHAR_SHEET"
	elif (( $(v -s char.str) = 17  ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+1" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+1" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "85 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "220 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "10 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "13" "$CHAR_SHEET"
	elif \
	[ "$(v -s char.class)" = "Fighter" -o \
	  "$(v -s char.class)" = "Paladin" -o \
	  "$(v -s char.class)" = "Ranger" ] && \
      [ ! "$(v -s char.race)"  = "Halfling" ] && \
        (( $(v -s char.str) = 18 ));then
		roll=$(d 1d100)
		if (( $roll <= 50 ));then
			xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+1" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+3" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "135 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "280 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "12 * 5%" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "20" "$CHAR_SHEET"
		elif (( $roll <= 75 ));then
			xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+2" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+3" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "160 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "305 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "13 * 5%" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "25" "$CHAR_SHEET"
		elif (( $roll <= 90 ));then
			xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+2" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+4" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "185 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "330 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "14 * 5%" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "30" "$CHAR_SHEET"
		elif (( $roll <= 99 ));then
			xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+2" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+5" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "235 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "380 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "15 * 5% (3 * 5% Magic)" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "35" "$CHAR_SHEET"
		elif (( $roll = 100 ));then
			xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+3" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+7" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "485 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "640 lb" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "16 * 5% (6 * 5% Magic)" "$CHAR_SHEET"
			xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "40" "$CHAR_SHEET"
		fi
	elif (( $(v -s char.str) = 18 ));then
		xmlstarlet ed -L -s "/char/other" -t elem -n "hit-bonus" -v "+1" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "damage-bonus" -v "+2" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "weight-max" -v "110 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "press-max" -v "255 lb" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "open-door-perc" -v "11 * 5%" "$CHAR_SHEET"
		xmlstarlet ed -L -s "/char/other" -t elem -n "bend-bars-perc" -v "16" "$CHAR_SHEET"
	fi

	dialog --stdout --title "Confirm Char Sheet (Additional Modifiers)" --yesno \
		"
		Race: $(v -s char.race)\n
		 Class: $(v -s char.class) \n
		 STR: $(v -s char.str) \n
		 DEX: $(v -s char.dex) \n
		 CON: $(v -s char.con) \n
		 INT: $(v -s char.int) \n
		 WIS: $(v -s char.wis) \n
		 CHA: $(v -s char.cha) \n
		 Other: \n
		     Max Level: $(v -s char.other.max-level) \n
		     Hit Bonus: $(v -s char.other.hit-bonus) \n
		     Damage Bonus: $(v -s char.other.damage-bonus) \n
		     Weight Max: $(v -s char.other.weight-max) \n
		     Press Max: $(v -s char.other.press-max) \n
		     Open Door %: $(v -s char.other.open-door-perc) \n
		     Bend Bars %: $(v -s char.other.bend-bars-perc)" 20 50
	confirm=$?

	if [ "$confirm" != 0 ];then
		mv "$CHAR_SHEET.bak" "$CHAR_SHEET"
		clear
		exit $confirm
	fi
	unset confirm


	clear
	echo "Character Creation Successfull"
}

function 3d6_in_order {
	xmlstarlet ed -L -u "/char/str" -v "$(d 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/dex" -v "$(d 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/con" -v "$(d 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/int" -v "$(d 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/wis" -v "$(d 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/cha" -v "$(d 3d6)" "$CHAR_SHEET"
}

function 3d6_advantage {
	xmlstarlet ed -L -u "/char/str" -v "$(d -a 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/dex" -v "$(d -a 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/con" -v "$(d -a 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/int" -v "$(d -a 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/wis" -v "$(d -a 3d6)" "$CHAR_SHEET"
	xmlstarlet ed -L -u "/char/cha" -v "$(d -a 3d6)" "$CHAR_SHEET"
}

function add_item_misc() {
	echo "What is the name of the item:"
	read name
	echo "What is the description of the item:"
	read desc
	echo "What is the quantity of the item:"
	read quant

	local id=$(echo "${name,,}" | sed 's/[[:punct:]]//g' | sed 's/\s\+/-/g')

	xmlstarlet ed -L -s "/char/inv/misc" -t elem -n itemTMP "$CHAR_SHEET"
	xmlstarlet ed -L -i "/char/inv/misc/itemTMP" -t attr -n "type" -v "misc" "$CHAR_SHEET"
	xmlstarlet ed -L -i "/char/inv/misc/itemTMP" -t attr -n "id" -v "$id" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/misc/itemTMP" -t elem -n "name" -v "$name" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/misc/itemTMP" -t elem -n "desc" -v "$desc" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/misc/itemTMP" -t elem -n "quant" -v "$quant" "$CHAR_SHEET"
	xmlstarlet ed -L -r "/char/inv/misc/itemTMP" -v "item" "$CHAR_SHEET"

	echo "$(v char.inv.misc.$id)"

	unset name
	unset desc
	unset quant
	unset id
}

function add_item_weapon() {
	echo "What is the name of the weapon:"
	read name
	echo "What is the description of the weapon:"
	read desc
	echo "What is the weight of the weapon:"
	read weight
	echo "What is the size of the weapon:"
	read size
	echo "What is the speed of the weapon:"
	read speed
	echo "What is the damage of the weapon:"
	read damage

	local id=$(echo "${name,,}" | sed 's/[[:punct:]]//g' | sed 's/\s\+/-/g')

	xmlstarlet ed -L -s "/char/inv/weapons" -t elem -n itemTMP "$CHAR_SHEET"
	xmlstarlet ed -L -i "/char/inv/weapons/itemTMP" -t attr -n "type" -v "weapon" "$CHAR_SHEET"
	xmlstarlet ed -L -i "/char/inv/weapons/itemTMP" -t attr -n "id" -v "$id" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/weapons/itemTMP" -t elem -n "name" -v "$name" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/weapons/itemTMP" -t elem -n "desc" -v "$desc" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/weapons/itemTMP" -t elem -n "weight" -v "$weight" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/weapons/itemTMP" -t elem -n "size" -v "$size" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/weapons/itemTMP" -t elem -n "speed" -v "$speed" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/weapons/itemTMP" -t elem -n "damage" -v "$damage" "$CHAR_SHEET"
	xmlstarlet ed -L -r "/char/inv/weapons/itemTMP" -v "item" "$CHAR_SHEET"

	echo "$(v char.inv.weapons.$id)"

	unset name
	unset desc
	unset weight
	unset size
	unset speed
	unset damage
}

function add_item_armor() {
	echo "What is the name of the armor:"
	read name
	echo "What is the description of the armor:"
	read desc
	echo "What is the new AC given by the armor:"
	read ac
	echo "What is the bonus to AC given by the armor:"
	read bonus

	local id=$(echo "${name,,}" | sed 's/[[:punct:]]//g' | sed 's/\s\+/-/g')

	xmlstarlet ed -L -s "/char/inv/armors" -t elem -n itemTMP "$CHAR_SHEET"
	xmlstarlet ed -L -i "/char/inv/armors/itemTMP" -t attr -n "type" -v "armor" "$CHAR_SHEET"
	xmlstarlet ed -L -i "/char/inv/armors/itemTMP" -t attr -n "id" -v "$id" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/armors/itemTMP" -t elem -n "name" -v "$name" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/armors/itemTMP" -t elem -n "desc" -v "$desc" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/armors/itemTMP" -t elem -n "ac" -v "$ac" "$CHAR_SHEET"
	xmlstarlet ed -L -s "/char/inv/armors/itemTMP" -t elem -n "bonus" -v "$bonus" "$CHAR_SHEET"
	xmlstarlet ed -L -r "/char/inv/armors/itemTMP" -v "item" "$CHAR_SHEET"

	echo "$(v char.inv.armors.$id)"

	unset name
	unset desc
	unset ac
	unset bonus

}

function add_item_spell() {
	continue
}

function add_language() {
	continue
}

function assign_value() {
	continue
}

function spend_money() {
	continue
}

function update_2e {
	continue
}


while [ True ];do
	if [ "$1" = "add" ];then
		if [ "$2" = "item" ];then
			add_item_misc
			break
		elif [ "$2" = "weapon" ];then
			add_item_weapon
			break
		elif [ "$2" = "armor" ];then
			add_item_armor
			break
		elif [ "$2" = "spell" ];then
			add_item_spell
			break
		elif [ "$2" = "language" ];then
			add_language
			break
		fi
	elif [ "$1" = "assign" ];then
		if [ -n "$2" -a -n "$3" ];then
			assign_value "$2" "$3"
		else
			echo "e: assign requires precisely two arguments"
		fi
		break
	elif [ "$1" = "spend" ];then
		spend_money "${@[2..]}"
		break
	elif [ "$1" = "update" ];then
		update_2e
		break
	elif [ "$1" = "init" ];then
		init
		if [ "$2" = "2e" ];then
			init_2e
		fi
		break
	elif [ -z "$1" ];then
		break
	else
		echo "e: $1 not a valid subcommand"
		break
	fi
done

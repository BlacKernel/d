#!/bin/bash

function xml() {
	xmllint --xpath "string($XML_PATH$1)" $CHAR_SHEET
}

function xmli() {
	xmllint --xpath "string($XML_PATH/item[@id=\"$1\"]/name)" $CHAR_SHEET
}

function xmlt() {
	xmllint --xpath "string($XML_PATH/item[@type=\"$1\"]/name)" $CHAR_SHEET
}

# Set CHAR_SHEET environment variable in .bashrc
XML_PATH=

STRING=
if [ "$1" = "-s" -o "$1" = "--string" ];then
	STRING=1
	shift 1
fi

for arg in $(echo "$1" | xargs -d'.'); do
	if [ -n "$(xml $arg)" ]; then
		XML_PATH="$XML_PATH$arg/"
	elif [ -n "$(xmli $arg)" ]; then
		XML_PATH="$XML_PATH/item[@id=\"$arg\"]/"
	elif [ -n "$(xmlt $arg)" ]; then
		XML_PATH="$XML_PATH/item[@type=\"$arg\"]/"
	fi
done

XML_PATH=${XML_PATH::-1}

if [ $STRING ];then
	xmllint --xpath "string($XML_PATH)" "$CHAR_SHEET"
else
	xmllint --xpath "$XML_PATH" "$CHAR_SHEET" | xmllint --format -
fi
